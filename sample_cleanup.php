<?php
/*
TECHNICAL TEST

PURPOSE: It is to test your understanding on object oriented programming
WHAT TO DO: Normalize the code as much as possible so that it is scalable in the future if there were more to add on for similar functions
HINT: U may need to normalize a few iteration
*/
class Test
{
	private $team;
	private $result;
	private $mapNumStr;
	private $match_summary;

	public function prepare_markets()
	{
		$this->match_summary = $match['pbp_stats']['match_summary'];
	
		$this->settle_map_draw_first_blood();
		$this->settle_map_first_tower_destroyed_location();
		$this->settle_map_first_inhibitor_destroyed_location();
	
		$this->settle_map_total_gold_diff();
		$this->settle_map_which_team_has_highest_gold();
		$this->settle_map_team_with_most_assist_point();
	
		$this->settle_map_total_dragon_slain_hdp();
		$this->settle_map_total_dragon_slain_over_under();
		$this->settle_map_total_dragon_slain_odd_even();
		$this->settle_map_total_baron_slain_hdp();
		$this->settle_map_total_baron_slain_over_under();
		$this->settle_map_total_baron_slain_odd_even();

		$this->settle_map_destroy_first_tower();
		$this->settle_map_to_kill_first_baron();
		$this->settle_map_to_kill_first_rift_herald();
	}

	/* ============================ */

	private function settle_map_draw_first_blood()
	{
		$player_id = $this->get_player_first_score('first_blood');
		if (!$player_id)
		{
			return;
		}
		$this->settle_map_score('TEAM TO DRAW FIRST BLOOD');
	}

	private function settle_map_first_tower_destroyed_location()
	{
		$tower_location = $this->get_objective_events_location('towers');
		if (!$tower_location)
		{
			return;
		}
		$this->settle_map_score('FIRST TOWER DESTROYED LOCATION');
	}

	private function settle_map_first_inhibitor_destroyed_location()
	{
		$inhibitor_location = $this->get_objective_events_location('inhibitors');
		if (!$inhibitor_location)
		{
			return;
		}
		$this->settle_map_score('FIRST INHIBITOR DESTROYED LOCATION');
	}

	private function settle_map_score($msg)
	{
		$team_id = $this->team['home']['id'];
		if (!$this->is_player_home_team($player_id))
		{
			$team_id = $this->team['away']['id'];
		}
		$this->result["{$this->mapNumStr} {$msg}"] = $team_id;
	}

	private function get_player_first_score($type)
	{
		return $this->match_summary['firsts'][$type]['player_id'];
	}

	private function get_objective_events_location($type)
	{
		return $this->match_summary['objective_events'][$type][0]['lane'];
	}

	private function is_player_home_team($player_id)
	{
		// check if player is home team
	}

	/* ============================ */

	private function get_player_roster($roster)
	{
		$roster_player_id = $this->match_summary[$roster]['players'][0]['player_id'];
		if (!$roster_player_id)
		{
			return;
		}
		return $this->get_home_away_team($roster_player_id);
	}

	private function get_player_points($team, $type)
	{
		return array_sum(
			array_column($this->match_summary[$team]['players'], $type)
		);
	}

	private function get_home_away_team($roster_player_id)
	{
		// check if player is home team
	}

	private function settle_map_total_gold_diff()
	{
		list($home, $away) = $this->get_player_roster('blue_roster');

		$home_gold_earned = $this->get_player_points($home, 'gold_earned');
		$away_gold_earned = $this->get_player_points($away, 'gold_earned'); 

		$total_gold_diff = abs(round(($home_gold_earned - $away_gold_earned) / 1000));
		$this->result["{$this->mapNumStr} TOTAL GOLD DIFFERENCE OVER/UNDER (IN THOUSANDS)"] = $total_gold_diff;
	}

	private function settle_map_which_team_has_highest_gold()
	{
		list($home, $away) = $this->get_player_roster('blue_roster');

		$home_gold_earned = $this->get_player_points($home, 'gold_earned');
		$away_gold_earned = $this->get_player_points($away, 'gold_earned'); 

		$team_id = $this->team['home']['id'];
		if ($away_gold_earned > $home_gold_earned)
		{
			$team_id = $this->team['away']['id'];
		}

		$this->result["{$this->mapNumStr} WHICH TEAM HAVE HIGHEST GOLD"] = $team_id;
	}

	private function settle_map_team_with_most_assist_point()
	{
		list($home, $away) = $this->get_player_roster('blue_roster');

		$home_assists = $this->get_player_points($home, 'assists');
		$away_assists = $this->get_player_points($away, 'assists'); 

		$team_id = $this->team['home']['id'];
		if ($away_assists > $home_assists)
		{
			$team_id = $this->team['away']['id'];
		}
		$this->result["{$this->mapNumStr} TEAM WITH MOST ASSIST POINT"] = $team_id;
	}

	/* ============================ */
	
	private function settle_map_total_dragon_slain_hdp()
	{
		$this->settle_map_total_slain('TOTAL DRAGON SLAIN HANDICAP', 'Dragons');
	}
	
	private function settle_map_total_dragon_slain_over_under()
	{
		$this->settle_map_total_slain('TOTAL DRAGON SLAIN OVER/UNDER', 'Dragons');
	}
	
	private function settle_map_total_dragon_slain_odd_even()
	{
		$this->settle_map_total_slain('TOTAL DRAGON SLAIN ODD/EVEN', 'Dragons');
	}
	
	private function settle_map_total_baron_slain_hdp()
	{
		$this->settle_map_total_slain('TOTAL BARON SLAIN HANDICAP', 'barons');
	}
	
	private function settle_map_total_baron_slain_over_under()
	{
		$this->settle_map_total_slain('TOTAL BARON SLAIN OVER/UNDER', 'barons');
	}
	
	private function settle_map_total_baron_slain_odd_even()
	{
		$this->settle_map_total_slain('TOTAL BARON SLAIN ODD/EVEN', 'barons');
	}

	private function settle_map_total_slain($msg, $enemy)
	{
		$events = $this->match_summary['objective_events'][$enemy];
		$scoreline = $this->common_event_score_line($events);
		$this->result["{$this->mapNumStr} {$msg}"] = $scoreline;
	} 

	private function common_event_score_line()
	{
		//
	}
	
	/*==================================*/

	private function settle_map_first_score($msg, $enemy)
	{
		$player_id = $this->get_player_first_score($enemy);
		
		if (!$player_id)
		{
			return;
		}
	
		$team_id = $this->team['home']['id'];
		if (!$this->is_player_home_team($player_id))
		{
			$team_id = $this->team['away']['id'];
		}
		$this->result["{$this->mapNumStr} {$msg}"] = $team_id;
	}
	
	private function settle_map_to_kill_first_rift_herald()
	{
		$this->settle_map_first_score('TO KILL FIRST RIFT HERALD', 'first_rift_herald');
	}
	
	private function settle_map_destroy_first_tower()
	{
		$this->settle_map_first_score('DESTROY FIRST TOWER', 'first_tower');
	}
	
	private function settle_map_to_kill_first_baron()
	{
		$this->settle_map_first_score('TO KILL FIRST BARON', 'first_baron');
	}	
	/* ========================================= */
}
