<?php

class Office_model extends CI_Model {
			
	public function get_office_employees()
	{
		$query = $this->db->select('
			offices.officeCode,
			offices.city,
			employees.employeeNumber,
			CONCAT(employees.firstName, " ", employees.lastName) name,
			employees.jobTitle
		')
		->from('offices')
		->join('employees', 'offices.officeCode = employees.officeCode')
		->order_by("offices.officeCode ASC, employees.employeeNumber ASC")
		->get();

		return $query->result_array();
	}
}
