<?php

class Employee_model extends CI_Model {
			
	public function get()
	{
		$query = $this->db
		->select('
			employeeNumber,
			CONCAT(firstName, " ", lastName) name,
			jobTitle,
			reportsTo
		')
		->from('employees')
		->order_by("employeeNumber", "asc")
		->get();

		return $query->result_array();
	}
}
