<?php

class Sales_model extends CI_Model {
			
	public function get_employee_sales($employee_number)
	{
		$sql = 'SELECT DISTINCT tbl.*
				FROM (
					SELECT 
						e.employeeNumber,
						CONCAT(e.firstName, " ", e.lastName) name,
						e.jobTitle,
						of.officeCode,
						of.city,
						SUM(IFNULL(od.quantityOrdered,0)) quantityOrdered,
						SUM((IFNULL(od.quantityOrdered,0) * od.priceEach)) sales,
						SUM((IFNULL(od.quantityOrdered,0) * od.priceEach) * (SUBSTRING_INDEX(p.productScale,":",1) / SUBSTRING_INDEX(p.productScale,":",-1))) commission,
						pl.productLine,
						pl.textDescription,
						p.productCode,
						p.productName,
					COUNT(c.customerNumber) numberOfCustomerBought
					FROM employees e
					LEFT JOIN offices of
						ON e.officeCode = of.officeCode
					LEFT JOIN customers c
						ON e.employeeNumber = c.salesRepEmployeeNumber
					LEFT JOIN orders o
						ON c.customerNumber = o.customerNumber
					LEFT JOIN orderDetails od
						ON o.orderNumber = od.orderNumber
					LEFT JOIN products p
						ON od.productCode = p.productCode
					LEFT JOIN productLines pl
						ON p.productLine = pl.productLine
					WHERE 1 = 1
					AND e.employeeNumber = IFNULL(?, e.employeeNumber)
					GROUP BY 
						e.employeeNumber, 
						pl.productLine, 
						p.productCode 
						WITH ROLLUP 
				) tbl
				ORDER BY 
					employeeNumber, 
					productLine, 
					CASE WHEN productLine IS NULL THEN 0 ELSE 1 END, 
					productCode, 
					CASE WHEN productCode IS NULL THEN 0 ELSE 1 END';

		$data = $this->db->query($sql, $employee_number);
		return $data->result_array();
	}
}
