<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('employee_model', 'employee');
	}

	public function index()
	{
		$employees = $this->employee->get();
		$org_structure = $this->build_org_structure($employees);

		echo json_encode($org_structure);
	}

	private function build_org_structure($employees, $reports_to = NULL) 
	{
		$structure = [];
		foreach ($employees as $key => $employee) {
			if ($employee['reportsTo'] == $reports_to) {
				unset($employees[$key]);
				$structure[] = $this->get_employee_under($employees, $employee);
			}
		}
		return $structure;
	}

	private function get_employee_under($employees, $employee) 
	{
		$employee_under = $this->build_org_structure($employees, $employee['employeeNumber']);
		$employee_under_details = [
			'employeeNumber' => $employee['employeeNumber'],
			'name'           => $employee['name'],
			'jobTitle'       => $employee['jobTitle'],
			'employeeUnder'  => []
		];

		if ($employee_under) {
			$employee_under_details['employeeUnder'] = $employee_under;
		}
		return $employee_under_details;
	}
}
