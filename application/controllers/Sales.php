<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller 
{
	private $sales = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('sales_model');
	}

	public function index($emp_number = NULL)
	{
		$employees = $this->sales_model->get_employee_sales($emp_number);

		$employee_number = NULL;
		$product_line = NULL;
		$employee_key = 0;
		$product_line_key = 0;

		foreach ($employees as $employee) {
			if (is_null($employee['employeeNumber'])) {
				continue;
			}
				
			if (is_null($employee['productLine']) && is_null($employee['productCode'])) {
				if (! is_null($employee_number)){
					$employee_key++;
					$product_line = NULL;
					$product_line_key = 0;
				}
				$employee_number = $this->add_employee($employee, $employee_key);
			} elseif (! is_null($employee['productLine']) && is_null($employee['productCode'])) {	
				if (! is_null($product_line)){
					$product_line_key++;
				}
				$product_line = $this->add_product_line($employee, $employee_key, $product_line_key);	
			} else {
				$this->add_product($employee, $employee_key, $product_line_key);
			}
		}
		echo json_encode($this->sales);
	}

	private function add_employee($employee, $key)
	{
		$this->sales[$key] = [
			'employeeNumber' => $employee['employeeNumber'],
			'name'           => $employee['name'],
			'jobTitle'       => $employee['jobTitle'],
			'officeCode'     => $employee['officeCode'],
			'city'           => $employee['city'],
			'totalCommision' => round($employee['commission'],2),
			'totalSales'     => round($employee['sales'],2)
		];
		return $employee['employeeNumber'];
	}

	private function add_product_line($employee, $employee_key, $product_line_key)
	{
		$this->sales[$employee_key]['productLines'][$product_line_key] = [
			'productLine'    => $employee['productLine'],
			'textDescription' => $employee['textDescription'],
			'commission'      => round($employee['commission'],2),
			'quantity'        => $employee['quantityOrdered'],
			'sales'           => round($employee['sales'],2)
		];
		return $employee['productLine'];
	}

	private function add_product($employee, $employee_key, $product_line_key)
	{
		$this->sales[$employee_key]['productLines'][$product_line_key]['products'][] = [
			'productCode'            => $employee['productCode'],
			'productName'            => $employee['productName'],
			'quantity'               => $employee['quantityOrdered'],
			'sales'                  => round($employee['sales'],2),
			'numberOfCustomerBought' => $employee['numberOfCustomerBought']
		];
	}
}
