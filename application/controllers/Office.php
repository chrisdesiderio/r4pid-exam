<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends CI_Controller 
{
	private $offices = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('office_model', 'office');
	}

	public function index()
	{	
		$office_employees = $this->office->get_office_employees();

		$office_code = NULL;
		$office_key = 0;

		foreach ($office_employees as $office_employee) {
			if ($office_employee['officeCode'] != $office_code) { 
				if(! is_null($office_code)){
					$office_key++;
				}
				$office_code = $this->add_office($office_employee, $office_key);
			}
			$this->add_employee($office_employee, $office_key);	
		}	
		echo json_encode($this->offices);
	}

	private function add_office($office_employee, $office_key)
	{
		$this->offices[$office_key] = [
			'officeCode' => $office_employee['officeCode'],
			'city'       => $office_employee['city']
		];
		return $office_employee['officeCode'];
	}

	private function add_employee($office_employee, $office_key)
	{
		$this->offices[$office_key]['employees'][] = [
			'employeeNumber' => $office_employee['employeeNumber'],
			'name'           => $office_employee['name'],
			'jobTitle'       => $office_employee['jobTitle']
		];	
	}
}
